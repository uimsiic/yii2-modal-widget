<?php

namespace junati\modalwidget;

use yii\web\AssetBundle;

class ModalWidgetAsset extends AssetBundle
{
    public $css = [

    ];
    public $js = [
        'js/ajax-modal-popup.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
      //  'app\themes\samarth\assets\SamarthThemeAsset'
    ];

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->sourcePath = __DIR__ . '/assets';
    }

}