function openModal(path, modalId, mode, listClass, pageReload) {
    $('#'+modalId).modal('show');
    registerModalCloseEvent(modalId);
    $.get(path, function(data, status, xhr) {
        if(xhr.status === 200){
            var form = $('#form-'+modalId);
            form.html(data);
            $('#'+modalId+'-loading-gif').hide();
            form.show();
            var originalFormId = form.find('form').attr('id');
            var action = (form.find('form').attr('action'));
            var newForm = $('#'+originalFormId);
            newForm.on('beforeSubmit', function (e) {
                e.preventDefault();
                if($(this).find('.has-error').length){
                    return false;
                }
                addModifyData(action, originalFormId, modalId, listClass, mode, pageReload);
                return false;
            });
        }
    });
}


function deleteRecord(element, path, listClass, pageReload) {
    var list_div;
    var c_status = confirm("Are you sure you want to delete the record. This action is not reversible once confirmed.");
    if (c_status == false) {
        return false;
    } else {
        $.ajax({
            url: path,
            dataType: 'html',
            method: 'post',
            success: function (data, status, xhr) {
                if (xhr.status == 200) {
                    if(pageReload == 1){
                        location.reload();
                    }else{
                        list_div = element.closest('.'+listClass);
                        list_div.innerHTML = data;
                        toastr.error("Record deleted successfully.", 'Record deleted');
                    }
                } else {
                    toastr.error(data, "Data deletion failed.");
                    return false;
                }
            },
            error: function ( data ) {
                toastr.error(data.responseText, "Data deletion failed.");
                return false;
            }
        });
        return true;
    }
}

function addModifyData(path, formId, modalId, listClass, mode, pageReload) {
    var list_div;
    var myForm = $('#'+formId);

    $.ajax({
        url: path,
        dataType: 'html',
        method: 'post',
        data: myForm.serialize(),
        success: function (data, status, xhr) {
            if (xhr.status == 200) {
                if(pageReload == 1){
                    location.reload();
                }else{
                    var modal = $('#'+modalId);
                    modal.modal('hide');
                    $('.modal-backdrop').hide();
                    if(mode == 0){
                        list_div = modal.nextAll('.'+listClass+':first');
                        list_div.html( data );
                        toastr.success("Data recorded successfully.", 'Record Created');
                    }else if(mode == 1){
                        list_div = modal.closest('.'+listClass);
                        list_div.html( data );
                        toastr.success("Data updated successfully.", 'Record Updated');
                    }

                }
            } else {
                toastr.error(data, "Data Recording Failed");
                return false;
            }
        },
        error: function ( data ) {
            toastr.error(data.responseText, "Data Recording Failed");
            return false;
        }
    });
    return true;
}

function registerModalCloseEvent(modalId){
    $('#'+modalId).on('hidden.bs.modal', function () {
        var formModalId = $('#form-'+modalId);
        formModalId.empty();
        formModalId.hide();
        $('#'+modalId+'-loading-gif').show();
    });
}

function registerView(view_path, listClass, modalId){
    if(view_path != false){
        $.get(view_path, function( data ) {
            var modal = $('#'+modalId);
            var list_div = modal.nextAll('.'+listClass+':first');
            list_div.html( data );
            list_div.show();
            modal.nextAll('.'+listClass+'-loading-gif:first').hide();
        });

    }
}