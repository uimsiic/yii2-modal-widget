<?php

use junati\modalwidget\ModalWidgetAsset;

ModalWidgetAsset::register($this);

?>
<?php if ($options['title']) { ?>
    <div class="row">
        <div class="col-md-6">
            <?= $options['title'] ?>
        </div>
        <div class="col-md-6 text-md-end">
            <?php if ($options['createPermission']) { ?>
                <button type="button" class="<?= $options['buttonClass'] ?>"
                        onclick="openModal('<?= $options['form_path'] ?>', '<?= $options['modal_id'] ?>', '<?= $options['mode'] ?>', '<?= $options['listClass'] ?>', '<?= $options['page_reload'] ?>')"><?= $options['name'] ?></button>
            <?php } ?>
        </div>
    </div>
<?php } else { ?>
    <?php if ($options['createPermission']) { ?>
        <button type="button" class="<?= $options['buttonClass'] ?>"
                onclick="openModal('<?= $options['form_path'] ?>', '<?= $options['modal_id'] ?>', '<?= $options['mode'] ?>', '<?= $options['listClass'] ?>', '<?= $options['page_reload'] ?>')"><?= $options['name'] ?></button>
    <?php } ?>
<?php } ?>


    <div class="modal" id="<?= $options['modal_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <?php if ($options['bsVersion'] == 5) { ?>
                <div class="modal-content">
                        <div id="<?= $options['modal_id'] ?>-loading-gif" style="display: block; text-align: center">
                            <?= $this->render('_loader') ?>
                        </div>
                        <div id="form-<?= $options['modal_id'] ?>" style="display: none">

                        </div>
                </div>
            <?php } else { ?>
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="<?= $options['modal_id'] ?>-loading-gif" style="display: block; text-align: center">
                            <?= $this->render('_loader') ?>
                        </div>
                        <div id="form-<?= $options['modal_id'] ?>" style="display: none">

                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>

    <div class="<?= $options['listClass'] ?>-loading-gif" style="display: block; text-align: center;">
        <?= $this->render('_loader') ?>
    </div>
    <br/>
    <div class="<?= $options['listClass'] ?>" style="display: none;">

    </div>


<?php

$this->registerJs("

    registerView('{$options['view_path']}', '{$options['listClass']}', '{$options['modal_id']}');

");

?>