<?php

use junati\modalwidget\ModalWidgetAsset;

ModalWidgetAsset::register($this);

?>

<?php if($options['updatePermission']){ ?>
    <button type="button" class="<?= $options['buttonClass'] ?>" onclick="openModal('<?= $options['form_path'] ?>', '<?= $options['modal_id'] ?>', '<?= $options['mode'] ?>', '<?= $options['listClass'] ?>', '<?= $options['page_reload'] ?>')"><?= $options['name'] ?></button>
<?php } ?>

<div class="modal" id="<?= $options['modal_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div id="<?= $options['modal_id'] ?>-loading-gif" style="display: block; text-align: center">
                    <?= $this->render('_loader') ?>
                </div>
                <div id="form-<?= $options['modal_id'] ?>" style="display: none">

                </div>
            </div>
        </div>
    </div>
</div>