<?php

use junati\modalwidget\ModalWidgetAsset;

ModalWidgetAsset::register($this);

?>

<?php if($options['deletePermission']){ ?>
    <button type="button" class="<?= $options['buttonClass'] ?>" onclick="deleteRecord(this, '<?= $options['form_path'] ?>', '<?= $options['listClass'] ?>', '<?= $options['page_reload'] ?>')">
        <?= $options['name'] ?>
    </button>
<?php } ?>