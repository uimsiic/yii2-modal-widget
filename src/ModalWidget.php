<?php

namespace junati\modalwidget;

use Yii;
use yii\base\Widget;

class ModalWidget extends Widget
{
    const CREATE = 0;
    const UPDATE = 1;
    const DELETE = 2;

    public $options;

    public function init()
    {
        parent::init();
        if(!isset($this->options['buttonClass'])){
            $this->options['buttonClass'] = 'btn btn-primary';
        }

        if(!isset($this->options['listClass'])){
            $this->options['listClass'] = 'widget_list_class_av';
        }

        if(!isset($this->options['view_path'])){
            $this->options['view_path'] = false;
        }

        if(!isset($this->options['modal_id'])){
            $this->options['modal_id'] = Yii::$app->security->generateRandomString(10);
        }

        if(!isset($this->options['page_reload'])){
            $this->options['page_reload'] = false;
        }

        if(!isset($this->options['title'])){
            $this->options['title'] = '';
        }

        if(!isset($this->options['bsVersion'])){
            $this->options['bsVersion'] = '4';
        }

        if(!isset($this->options['createPermission'])){
            $this->options['createPermission'] = true;
        }

        if(!isset($this->options['updatePermission'])){
            $this->options['updatePermission'] = true;
        }

        if(!isset($this->options['deletePermission'])){
            $this->options['deletePermission'] = true;
        }

    }

    public function run()
    {
        if($this->options['mode'] == 0){
            return $this->render('create_form', ['options' => $this->options]);
        }elseif($this->options['mode'] == 1){
            return $this->render('update_form', ['options' => $this->options]);
        }elseif($this->options['mode'] == 2){
            return $this->render('delete_form', ['options' => $this->options]);

        }
    }
}