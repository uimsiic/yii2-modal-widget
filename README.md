Modal Widget for Add More functionality
=======================================
This is a modal widget for achieving the add more functionality

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist junati/yii2-modal-widget "*"
```

or add

```
"junati/yii2-modal-widget": "*"
```

to the require section of your `composer.json` file.


Installation From Bitbucket
---------------------------

Add

```
"junati/yii2-modal-widget": "dev-master"
```

to the require section of your `composer.json` file and 

Add

```php

"repositories": [
    {
        "type": "git",
        "url": "https://vishal_iic@bitbucket.org/uimsiic/yii2-modal-widget.git"
    }
]
```

at the end of your `composer.json` file.

**Note: replace `vishal_iic` with your user name.**

Widget Options
--------------

**_mode_:** is a required parameter. It tells the widget in which mode it is being used (ModalWidget::CREATE,ModalWidget::UPDATE,ModalWidget::DELETE).

**_modal_id_:** sets the id of the modal. If it is not provided, the widget will provide a default id to the modal.

**_name_:** is a required parameter. It sets the name of the button.

**_buttonClass_:** sets the class property of the button. Default is '**btn btn-primary**'.

**_form_path_:** is a required parameter. It tells the widget, from where to load the form in case of **create** and **update**. In **delete** mode, it tells the widget where to send the delete request.

**_view_path_:** takes a url as input from where the view should be generated. If this value is not passed, no view will be loaded which is the case in **update** and **delete** mode.

**_page_reload_:** takes value as **true** or **false**. If the value is **true**, the the page will reload after the operation. If the value is **false**, only the view, if passed to the widget, will reload.


Usage
-----

**Note: You must pass a unique form id to all the form views.**

The extension has three modes  :

**1) Create**

In view file use the widget as defined below:


```php
<?php 

use junati\modalwidget\ModalWidget; 

echo ModalWidget::widget([
         'options' => [
             'mode' => ModalWidget::CREATE,
             'modal_id' => 'ug_create_modal_id',
             'name' => Yii::t('app', 'Add Under-Graduate Details'),
             'buttonClass' => 'btn btn-success',
             'form_path' => Url::to(['/jiacademic/ug/create']),
             'view_path' => Url::to(['/jiacademic/app/ug-details', 'id' => $id]),
             'page_reload' => true
         ]
     ])

?>
```

Controller/Action name from where the form is to be rendered.

**Note: view should be returned as renderAjax()**

```php
<?php 

public function actionCreate()
{
    $model = new UgDetails();

    return $this->renderAjax('create', [
        'model' => $model,
    ]);
}

?>
```

Controller/Action name from where the submitted form is to be viewed after successful submission.

**Note: view should be returned as renderAjax() in the Controller/Action**

```php
<?php 

public function actionUgDetails($id)
{
    $hashId = SecurityHelper::validateData($id);
    if($hashId){
        $data = UgDetails::find()->where(['user_id' => $hashId])->orderBy(['id' => SORT_DESC])->all();
        return $this->renderAjax('ug_index_view', ['data' => $data]);
    }else{
        throw new BadRequestHttpException(Yii::t('app', 'Invalid request.'));
    }
}

?>
```

Controller/Action name where the form is being saved with the submission logic.

```php
<?php 

public function actionAjaxSubmit()
{
    if ($_POST) {
        $model = new UgDetails();
        $model->load($_POST);
        if ($model->result_type == 1) {
            $model->percentage = $model->tempPercentage;
        } else {
            $model->percentage = $model->tempGrade;
        }
        $model->user_id = Yii::$app->user->identity->id;
        if ($model->validate()) {
            $model->save();
            Yii::$app->response->format = 'html';
            Yii::$app->response->statusCode = 200;
            $html = Yii::$app->runAction('/jiacademic/app/ug-details', ['id' => SecurityHelper::hashData(Yii::$app->user->id)]);
            return $html;
        } else {
            $str = ArrayHelper::getColumn($model->getErrors(), 0, true);
            $err = '';
            foreach ($str as $key => $value) {
                $err .= '<li>' . $model->getAttributeLabel($key) . ':' . $value . '</li>';
            }
            Yii::$app->response->statusCode = 400;
            return json_encode($err);
        }
    } else {
        Yii::$app->response->statusCode = 400;
        return '<li>'.Yii::t('app', 'Invalid form data.').'</li>';
    }
}

?>
```

**HTML Output**

![alt text](http://10.107.77.52:8090/download/attachments/1736838/Screenshot%202019-04-04%20at%207.26.59%20PM.png?version=1&modificationDate=1554386229000&api=v2&effects=drop-shadow)

**2) Update**

In view file use the widget as defined below:

```php
<?php 

use junati\modalwidget\ModalWidget; 

echo ModalWidget::widget([
    'options' => [
        'mode' => ModalWidget::UPDATE,
        'modal_id' => 'ug_update_modal_id_'.$key,
        'name' => Yii::t('app', 'Modify'),
        'buttonClass' => 'badge badge-success',
        'form_path' => Url::to(['/jiacademic/ug/update', 'id' => SecurityHelper::hashData($record->id)]),
    ]
]);

?>
```

Controller/Action name to view the form in Update Mode.

**Note: view should be returned as renderAjax()**

```php
<?php 

public function actionUpdate($id)
{
    $model = $this->findModel(SecurityHelper::validateData($id));

    return $this->renderAjax('update', [
        'model' => $model,
    ]);
}

?>
```

Controller/Action name where the form is being saved with the update logic.

```php
<?php 

public function actionAjaxUpdate($id)
{
    if ($_POST) {
        $model = $this->findModel(SecurityHelper::validateData($id));
        if (!empty($model)) {
            $model->load($_POST);
            if ($model->result_type == 1) {
                $model->percentage = $model->tempPercentage;
            } else {
                $model->percentage = $model->tempGrade;
            }
            $model->user_id = Yii::$app->user->identity->id;
            if ($model->validate()) {
                $model->save();
                Yii::$app->response->format = 'html';
                Yii::$app->response->statusCode = 200;
                $html = Yii::$app->runAction('/jiacademic/app/ug-details', ['id' => SecurityHelper::hashData(Yii::$app->user->id)]);
                return $html;
            } else {
                $str = ArrayHelper::getColumn($model->getErrors(), 0, true);
                $err = '';
                foreach ($str as $key => $value) {
                    $err .= '<li>' . $model->getAttributeLabel($key) . ':' . $value . '</li>';
                }
                Yii::$app->response->statusCode = 400;
                return json_encode($err);
            }
        }else{
            Yii::$app->response->statusCode = 400;
            return '<li>'.Yii::t('app', 'Invalid form data.').'</li>';
        }
    } else {
        Yii::$app->response->statusCode = 400;
        return '<li>'.Yii::t('app', 'Invalid form data.').'</li>';
    }

}

?>
```
**HTML Output**

![alt text](http://10.107.77.52:8090/download/attachments/1736838/Screenshot%202019-04-04%20at%207.39.23%20PM.png?version=1&modificationDate=1554386998000&api=v2&effects=drop-shadow)

**3) Delete**

In view file use the widget as defined below:

```php
<?php 

use junati\modalwidget\ModalWidget; 

echo ModalWidget::widget([
    'options' => [
        'mode' => ModalWidget::DELETE,
        'name' => Yii::t('app', 'Delete'),
        'buttonClass' => 'badge badge-danger',
        'form_path' => Url::to(['/jiacademic/ug/delete', 'id' => SecurityHelper::hashData($record->id)]),
    ]
]);

?>
```

Controller/Action name to delete the record

```php
<?php 

public function actionDelete($id)
{
    $model = $this->findModel(SecurityHelper::validateData($id));
    if ($model) {
        if ($model->delete()) {
            Yii::$app->response->format = 'html';
            Yii::$app->response->statusCode = 200;
            $html = Yii::$app->runAction('/jiacademic/app/ug-details', ['id' => SecurityHelper::hashData(Yii::$app->user->id)]);
            return $html;
        } else {
            Yii::$app->response->statusCode = 400;
            return json_encode(['<li>'.Yii::t('app', 'Invalid form data.').'</li>']);
        }
    } else {
        Yii::$app->response->statusCode = 400;
        return json_encode(['<li>'.Yii::t('app', 'Invalid form data.').'</li>']);
    }
}

?>
```
**HTML Output**

![alt text](http://10.107.77.52:8090/download/attachments/1736838/Screenshot%202019-04-04%20at%207.44.40%20PM.png?version=1&modificationDate=1554387300000&api=v2&effects=drop-shadow)